(function($) {
window.cms_embed = function(html, container, fragment) {
  if(typeof(html) == 'undefined') {
    return; // no content???
  }
  // where to embed the content? defaults to 'body'
  container = typeof(container) == 'undefined' ? $('body') : $(container);
  // which subset (selector) of content to embed. defaults to all.
  var content = $(html);
  // filter content for selector = fragment
  if(typeof(fragment) != 'undefined') {
    content = content.find(fragment).add(content.filter(fragment));
  }

  if(content.size() == 0) {
    content = $('<span class="cms-embed message error">No matching content found ... </span>');
  }

  // replace a[href] if not absolute
  _make_urls_absolute(content, 'a', 'href');
  // replace img[src] if not absolute
  _make_urls_absolute(content, 'img', 'src');
  // replace form[action] if not absolute
  _make_urls_absolute(content, 'form', 'action');
  // instrument form submissions
  _instrument_form_submissions(content, container);

  container.html(content); // embed
  // instrument all a.href links
  container.find('a').click({container: container}, function(event) {
    var a = $(this);
    var href = a.attr('href');
    _generate_jsonp(event.data.container, href);
    return false;
  });
}

function _generate_id() {
  var id;
  while((elem = document.getElementById('id-' + _generate_id.counter))) {
    _generate_id.counter++;
  }
  return 'id-' + _generate_id.counter;
}

function _generate_jsonp(container, href) {
  var src = href + (href.indexOf('?') == -1 ? '?' : '&' )+ '_em[cb]=&_em[]=%23' + container.attr('id');
  var fragment = container.attr('data-cms-fragment');
  if(fragment) src += '&_em[]=' + escape(fragment);
  var embed_js = container.attr('data-cms-embedjs');
  if(embed_js) src += '&_js=1';
  var embed_css = container.attr('data-cms-embedcss');
  if(embed_css) src += '&_css=1';
  container.after('<script type="text/javascript" src="' + src + '"></script>');
}

function _make_urls_absolute(content, tag, attribute) {
  var selector = tag + ':not([' + attribute + '^="http"])';
  // filter children, as well as self and add the two sets
  content.find(selector).add(content.filter(selector)).each(function() {
    var attribute_val = $(this).attr(attribute);
    $(this).attr(attribute, cms_embed_baseurl + attribute_val);
  });
}

function _instrument_form_submissions(content, container) {
  var selector = 'form[method=get],form[method=GET]';
  // filter children, as well as self and add the two sets
  content.find(selector).add(content.filter(selector)).submit(
    { container: container },
    function(event) {
      var form = $(this);
      var container = event.data.container;
      var params = form.serialize();
      var embed_href = form.attr('action');
      embed_href += embed_href.indexOf('?') == -1 ? '?' : '&';
      embed_href += params;
      _generate_jsonp(container, embed_href);
      return false;
    }
  );
}

_generate_id.counter = 0;

$(document).ready(function() {
  $(document).find('*[data-cms-embed]').each(function() {
    this.id || (this.id = _generate_id());
    var container = $(this);
    var embed_href = container.attr('data-cms-embed');
    _generate_jsonp(container, embed_href);
  });
});

})(jQuery);
